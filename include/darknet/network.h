/*
 * darknet C++ Wrapper
 * Copyright (C) 2018 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>
#include <string>
#include <vector>

// darknet
#include <darknet/detection.h>
#include <darknet/image.h>


namespace cdarknet
{

class network;

}


namespace darknet
{


class network
{

    protected:

        std::unique_ptr<cdarknet::network> m_network;
        float m_thresh = 0.5f;
        float m_hier_thresh = 0.5f;
        float m_nms = 0.45f;
        int m_class_count = 80;

    public:

        network();
        network(const std::string& cfgfile, const std::string& weightfile, int clear = 0);
        virtual ~network();

        virtual void load(const std::string& cfgfile, const std::string& weightfile, int clear = 0);
        virtual bool is_loaded() const;
        virtual void set_batch(int b);

        void thresh(float value);
        float thresh();
        void hier_thresh(float value);
        float hier_thresh();
        void nms(float value);
        float nms();
        void class_count(int value);
        int class_count() const;

        std::vector<darknet::detection> predict(const darknet::image& input_image) const;

        operator cdarknet::network*();
        operator cdarknet::network*() const;

};


}
