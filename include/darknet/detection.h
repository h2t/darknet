/*
 * darknet C++ Wrapper
 * Copyright (C) 2018 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <christian.dreher@student.kit.edu>
 * @date       2018
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <map>


namespace darknet
{


class detection
{

    protected:

        std::map<int, float> m_candidates;

        float m_x;
        float m_y;
        float m_w;
        float m_h;

    public:

        /**
         * @brief Default constructor of a darknet detection
         */
        detection();

        /**
         * @brief Destructor
         */
        virtual ~detection();

        /**
         * @brief Adds a candidate given by class index and the corresponding certainty
         * @param class_index Class index of the candidate
         * @param certainty Certainty of the candidate
         */
        void add_candidate(int class_index, float certainty);

        /**
         * @brief Returns the candidates for this detection
         *
         * A candidate is a pair (class index, certainty), returned as std::map. The returned map only has entries for
         * classes with a certainty > thresh (the thresh parameter the network was initialised with)
         *
         * @return Map or (class_index, certainty)
         */
        std::map<int, float> candidates() const;

        /**
         * @brief Sets the X value of the center of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the input images' width
         *
         * @param value New X value for the center of the detections' bounding box
         */
        void x(float value);

        /**
         * @brief Gets the X value of the center of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the input images' width
         *
         * @return X value of the center of the detections' bounding box
         */
        float x() const;

        /**
         * @brief Sets the Y value of the center of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the original images' height
         *
         * @param value New Y value for the center of the detections' bounding box
         */
        void y(float value);

        /**
         * @brief Gets the Y value of the center of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the original images' height
         *
         * @return Y value of the center of the detections' bounding box
         */
        float y() const;

        /**
         * @brief Sets the width of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the input images' width
         *
         * @param value New width of the detections' bounding box
         */
        void w(float value);

        /**
         * @brief Gets the width of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the input images' width
         *
         * @return Width of the detections' bounding box
         */
        float w() const;

        /**
         * @brief Sets the height of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the input images' height
         *
         * @param value New height of the detections' bounding box
         */
        void h(float value);

        /**
         * @brief Gets the height of the detections' bounding box
         *
         * The value must be in the interval [0, 1] and is interpreted relative to the input images' height
         *
         * @return Height of the detections' bounding box
         */
        float h() const;

};


}
