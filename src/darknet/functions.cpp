/*
 * darknet C++ Wrapper
 * Copyright (C) 2019 Christian R. G. Dreher
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * @package    darknet
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2019
 * @copyright  https://www.gnu.org/licenses/gpl-3.0.html
 *             GNU General Public License
 */


#include <darknet.h>


// STD/STL
#include <cmath>
#include <tuple>

// C darknet
#include <cdarknet/darknet.h>


// Copied from Darknet's source code to reproduce object-associated colors, replaced C functions
// with C++.
int
get_color(int c, int x, int max)
{
    double colors[6][3] = { {1,0,1}, {0,0,1}, {0,1,1}, {0,1,0}, {1,1,0}, {1,0,0} };
    double ratio = static_cast<double>(x) / max * 5;
    int i = static_cast<int>(std::floor(ratio));
    int j = static_cast<int>(std::ceil(ratio));
    ratio -= i;
    double r = (1 - ratio) * colors[i][c] + ratio * colors[j][c];
    return static_cast<int>(r * 255);
}


std::tuple<int, int, int>
darknet::get_color(int class_index, int class_count)
{
    const int seed = 123457;
    const int offset = class_index * seed % class_count;

    return {
        ::get_color(2, offset, class_count),
        ::get_color(1, offset, class_count),
        ::get_color(0, offset, class_count)
    };
}


#ifdef GPU
void
darknet::cuda_set_device(int id)
{
    ::cuda_set_device(id);
}
#endif
